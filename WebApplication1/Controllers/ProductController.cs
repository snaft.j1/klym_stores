using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Entities;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private productSevice _productSevice = new productSevice();

        [HttpGet("{ean}/{curr}")]
        public Task<List<Product>> getAll(string ean, string curr)
        {
            return _productSevice.getAll(ean, curr);
        }

        [HttpPost]
        public async Task<IActionResult> addProduct(Product prodcutObj)
        {
            try
            {
                return Ok(_productSevice.addProduct(prodcutObj));
            }
            catch (Exception e)
            {
                return NoContent();
            }

        }

        [HttpPut("{ean}")]
        public async Task<IActionResult> updateProduct(string ean, Product product)
        {
            try
            {
                return Ok(_productSevice.updateProduct(product));
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }

    }
}
