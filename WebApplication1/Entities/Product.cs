namespace WebApplication1.Entities
{
    public class Product
    {
        public string ean { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public double price { get; set; }

    }
}
