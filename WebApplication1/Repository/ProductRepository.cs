using WebApplication1.Entities;

namespace WebApplication1.Repository
{
    public interface ProductRepository
    {
        public Product save(Product product);
        public Product update(Product product);
        public Product findById(string ean);
    }
}
