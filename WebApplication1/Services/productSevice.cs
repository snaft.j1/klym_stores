using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebApplication1.dto;
using WebApplication1.Entities;
using WebApplication1.Repository;


namespace WebApplication1.Services
{
    public class productSevice
    {
       // private readonly
        private List<Product> _products = new List<Product>();
        private List<Product> _productsByCurrency = new List<Product>();
        private ProductRepository _productRepository;

        HttpClient _client = new HttpClient();

        public productSevice(ProductRepository repository)
        {
            _productRepository = repository;
        }

        public async Task<List<Product>> getAll(string ean, string curr)
        {
            //1. http request to currencies service
            HttpResponseMessage responseMessage = await _client.GetAsync("localhost:1080/USD/to/" + curr);
            if (responseMessage.IsSuccessStatusCode)
            {
                var result = await responseMessage.Content.ReadAsStringAsync();
                CurrencyExchange currencyExchange = JsonSerializer.Deserialize<CurrencyExchange>(result);
                foreach (var product in _products)
                {
                    product.price = product.price * currencyExchange.rate;
                    _productsByCurrency.Add(product);
                }
            }

            return _productsByCurrency;
        }

        public Product addProduct(Product product)
        {
            //Check for null product
            if (product == null)
            {
                throw new Exception("One Product is required");
            }

            //add product to in memory collection
            _products.Add(product);
            //persist object
            var productEntity = _productRepository.save(product);

            return productEntity;
        }

        public Product updateProduct(Product product)
        {

            //1. find product exist
            var productEntity = _productRepository.findById(product.ean);
            if (productEntity == null)
            {
                throw new Exception("Product dont exist");
            }
            //2. if existe update product on database
            return _productRepository.update(product);
            //3. if not exist return exception.
        }

    }
}
